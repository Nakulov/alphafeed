package com.nakulov.feed.presentation.presenter

interface Presenter {

    fun pause()

    fun resume()

    fun destroy()

}