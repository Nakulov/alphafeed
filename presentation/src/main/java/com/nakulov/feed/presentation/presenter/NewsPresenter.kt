package com.nakulov.feed.presentation.presenter

import com.nakulov.feed.data.models.Feed
import com.nakulov.feed.presentation.di.PerFragment
import com.nakulov.feed.presentation.view.NewsView
import io.realm.Realm
import javax.inject.Inject

@PerFragment
class NewsPresenter @Inject constructor() : Presenter {

    private val realm = Realm.getDefaultInstance()
    private var newsView: NewsView? = null

    fun attachView(newsView: NewsView) {
        this.newsView = newsView
    }

    fun initialize() {
        this.setRealm()
        this.getNews()
    }

    private fun getNews() {
        val news = realm.where(Feed::class.java).findAll().map { it.link }
        this.showNews(news)
    }

    private fun setRealm() {
        this.newsView?.setRealm(realm)
    }

    private fun showNews(news: Collection<String>) {
        this.newsView?.setNews(news)
    }

    private fun showErrorMessage(message: String) {
        this.newsView?.showError(message)
    }

    override fun pause() {

    }

    override fun resume() {

    }

    fun detachView() {
        this.newsView = null
    }

    override fun destroy() {
        this.realm.close()
    }
}