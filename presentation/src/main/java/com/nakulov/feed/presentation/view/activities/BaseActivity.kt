package com.nakulov.feed.presentation.view.activities

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.nakulov.feed.presentation.FeedApp.Companion.applicationComponent
import com.nakulov.feed.presentation.calculateDensity
import com.nakulov.feed.presentation.di.HasComponent
import com.nakulov.feed.presentation.di.components.ActivityComponent
import com.nakulov.feed.presentation.di.components.DaggerActivityComponent
import com.nakulov.feed.presentation.di.modules.ActivityModule

abstract class BaseActivity : AppCompatActivity(), HasComponent<ActivityComponent> {

    protected lateinit var activityComponent: ActivityComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        calculateDensity(this)

        this.initializeInjectors()
        this.activityComponent.inject(this)
    }

    private fun initializeInjectors() {
        this.activityComponent = DaggerActivityComponent.builder()
                .applicationComponent(applicationComponent)
                .activityModule(ActivityModule(this))
                .build()
    }

    fun presentFragment(containerViewId: Int, fragment: Fragment, removeLast: Boolean = false) {
        val last = supportFragmentManager.backStackEntryCount

        if (last > 0) {
            val tag = supportFragmentManager.getBackStackEntryAt(last - 1).name

            val currentFragment = supportFragmentManager.findFragmentByTag(tag)

            if (fragment.javaClass.canonicalName == currentFragment.javaClass.canonicalName) return
        }

        if (removeLast) supportFragmentManager.popBackStack()

        val stackSize = supportFragmentManager.backStackEntryCount

        val fragmentTransaction = supportFragmentManager.beginTransaction()

        if (stackSize == 0) fragmentTransaction.add(containerViewId, fragment, fragment.javaClass.simpleName)
        else fragmentTransaction.replace(containerViewId, fragment, fragment.javaClass.simpleName)

        fragmentTransaction.addToBackStack(fragment.javaClass.simpleName)
        fragmentTransaction.commit()

    }

    override fun getComponent(): ActivityComponent = activityComponent

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1) finish() else super.onBackPressed()
    }

}