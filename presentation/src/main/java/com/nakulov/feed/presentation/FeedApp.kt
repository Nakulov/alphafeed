package com.nakulov.feed.presentation

import android.app.Application
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.nakulov.feed.presentation.di.components.ApplicationComponent
import com.nakulov.feed.presentation.di.components.DaggerApplicationComponent
import com.nakulov.feed.presentation.di.modules.ApplicationModule
import io.realm.Realm
import io.realm.RealmConfiguration

var applicationHandler: Handler = Handler(Looper.getMainLooper())

class FeedApp : Application() {

    private val DB_NAME = "feed.realm"

    companion object {
        lateinit var applicationComponent: ApplicationComponent
    }

    override fun onCreate() {
        super.onCreate()
        this.initializeInjectors()
        this.initRealm()
    }

    private fun initializeInjectors() {
        applicationComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(ApplicationModule(this))
                .build()
    }

    private fun initRealm() {
        Realm.init(this)

        val configuration = RealmConfiguration.Builder()
                .name(DB_NAME)
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build()

        Realm.setDefaultConfiguration(configuration)

        val realm = Realm.getDefaultInstance()
        realm.use { Log.i("RealmPath", realm.path) }
    }
}