package com.nakulov.feed.presentation.di

interface HasComponent<out C> {

    fun getComponent() : C

}