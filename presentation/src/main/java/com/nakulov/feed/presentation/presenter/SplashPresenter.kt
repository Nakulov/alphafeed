package com.nakulov.feed.presentation.presenter

import com.nakulov.feed.data.NotificationCenter
import com.nakulov.feed.data.NotificationCenterImpl.NotificationObserverDelegate
import com.nakulov.feed.data.cache.Cache
import com.nakulov.feed.data.needUpdateFeed
import com.nakulov.feed.presentation.di.PerFragment
import com.nakulov.feed.presentation.view.SplashView
import javax.inject.Inject

@PerFragment
class SplashPresenter @Inject constructor(
        private val notification: NotificationCenter,
        private val cache: Cache
): Presenter, NotificationObserverDelegate {

    private var splashView: SplashView? = null

    fun attachView(splashView: SplashView) {
        this.splashView = splashView
        this.addObservers()
    }

    private fun addObservers() {
        this.notification.addObserver(needUpdateFeed, this)
    }

    override fun notifyObserver(id: Int, vararg arguments: Any) {
        when(id) {
            needUpdateFeed -> {
                cache.setFirstLaunch(false)
                splashView?.showFeed()
            }
        }
    }

    override fun pause() {

    }

    override fun resume() {

    }

    private fun removeObservers() {
        this.notification.removeObserver(needUpdateFeed, this)
    }

    fun detachView() {
        this.removeObservers()
        this.splashView = null
    }

    override fun destroy() {

    }
}