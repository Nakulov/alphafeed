package com.nakulov.feed.presentation.view

interface FeedListView : LoadDataView {

    fun renderFeedList()

}