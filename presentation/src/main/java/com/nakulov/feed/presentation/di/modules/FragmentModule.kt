package com.nakulov.feed.presentation.di.modules

import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.nakulov.feed.presentation.di.PerFragment
import dagger.Module
import dagger.Provides

@Module
class FragmentModule(private val fragment: Fragment){

    @Provides
    @PerFragment
    fun provideActivity() = fragment.activity as AppCompatActivity

}