package com.nakulov.feed.data.net

import com.nakulov.feed.data.models.Feed

interface Repository {

    suspend fun loadFeed() : ArrayList<Feed>

}