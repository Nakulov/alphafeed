package com.nakulov.feed.data.net

import com.nakulov.feed.data.BuildConfig
import kotlinx.coroutines.experimental.CancellableContinuation
import kotlinx.coroutines.experimental.suspendCancellableCoroutine
import me.toptas.rssconverter.RssConverterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level.BODY
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit.MILLISECONDS
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ApiConnection @Inject constructor() {

    val service = createService()

    private fun createService(): ApiRepository =
            Retrofit.Builder()
                    .baseUrl(BuildConfig.apiUrl)
                    .addConverterFactory(RssConverterFactory.create())
                    .client(createClient())
                    .build()
                    .create(ApiRepository::class.java)

    @Suppress("ConstantConditionIf")
    private fun createClient(): OkHttpClient =
            OkHttpClient.Builder()
                    .addInterceptor(HttpLoggingInterceptor().setLevel(BODY))
                    .readTimeout(10000, MILLISECONDS)
                    .connectTimeout(15000, MILLISECONDS)
                    .build()

}

suspend fun <T> Call<T>.await(): T = suspendCancellableCoroutine { continuation ->

    continuation.invokeOnCompletion { if (continuation.isCancelled) cancel() }

    val callback = object : Callback<T> {
        override fun onFailure(call: Call<T>, t: Throwable) = continuation.tryToResume {
            throw Throwable(t.message)
        }

        override fun onResponse(call: Call<T>, response: Response<T>) = continuation.tryToResume {
            /*response.isSuccessful || throw NetworkConnectionException("Http error with code: ${response.code()}")*/
            val errorBody = response.errorBody()

            if (errorBody != null) throw Throwable(errorBody.string())
            else response.body() ?: throw Throwable("Response body is null")
        }
    }

    enqueue(callback)
}

private inline fun <T> CancellableContinuation<T>.tryToResume(getter: () -> T) {
    this.isActive || return

    try {
        resume(getter())
    } catch (t: Throwable) {
        resumeWithException(t)
    }

}