package com.nakulov.feed.data.cache

interface Cache {

    fun setFirstLaunch(firstTime: Boolean)

    fun isFirstLaunch() : Boolean

    fun getLastUpdateTime() : Long

    fun putLastUpdateTime(time: Long)

}