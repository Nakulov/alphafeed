package com.nakulov.feed.data.cache

import com.google.gson.Gson
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Serializer @Inject constructor() {

    private val gson = Gson()

    fun serialize(any: Any) = gson.toJson(any)

    fun <T> deserialize(string: String, clazz: Class<T>) : T = gson.fromJson(string, clazz)
}