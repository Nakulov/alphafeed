package com.nakulov.feed.data.cache

import javax.inject.Inject
import javax.inject.Singleton

private const val CLIENT_CONFIG_FILE_NAME = "com.nakulov.feed.CLIENT_CONFIG"

private const val FIRST_LAUNCH = "firstLaunch"
private const val LAST_UPDATE_TIME = "lastUpdateTime"

@Singleton
class CacheImpl @Inject constructor(
        private val cacheManager: CacheManager,
        private val serializer: Serializer
) : Cache {
    override fun setFirstLaunch(firstTime: Boolean) {
        this.cacheManager.writeToPreference(CLIENT_CONFIG_FILE_NAME, FIRST_LAUNCH, firstTime)
    }

    override fun isFirstLaunch(): Boolean {
        return cacheManager.readFromPreference(CLIENT_CONFIG_FILE_NAME, FIRST_LAUNCH, true)
    }

    override fun getLastUpdateTime(): Long {
        return cacheManager.readFromPreference(CLIENT_CONFIG_FILE_NAME, LAST_UPDATE_TIME, 0L)
    }

    override fun putLastUpdateTime(time: Long) {
        this.cacheManager.writeToPreference(CLIENT_CONFIG_FILE_NAME, LAST_UPDATE_TIME, time)
    }
}